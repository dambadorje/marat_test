
$(document).ready(function() {
  $("#lightSlider").lightSlider({
    item: 1,
    prevHtml : '<div class="slider_arrow slider_arrow__left"></div>',
    nextHtml : '<div class="slider_arrow slider_arrow__right"></div>',
    pager: false
  });
});



// Uncomment to enable Bootstrap tooltips
// https://getbootstrap.com/docs/4.0/components/tooltips/#example-enable-tooltips-everywhere
// $(function () { $('[data-toggle="tooltip"]').tooltip(); });

// Uncomment to enable Bootstrap popovers
// https://getbootstrap.com/docs/4.0/components/popovers/#example-enable-popovers-everywhere
// $(function () { $('[data-toggle="popover"]').popover(); });
